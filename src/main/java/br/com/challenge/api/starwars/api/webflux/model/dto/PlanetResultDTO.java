package br.com.challenge.api.starwars.api.webflux.model.dto;

import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PlanetResultDTO<T> {
	private int count;
	private String next;
	private String previous;
	private List<T> results;
}

