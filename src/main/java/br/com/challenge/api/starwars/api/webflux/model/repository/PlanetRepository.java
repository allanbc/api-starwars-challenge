package br.com.challenge.api.starwars.api.webflux.model.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import reactor.core.publisher.Flux;

public interface PlanetRepository extends ReactiveMongoRepository<Planet, String> {

	Flux<Planet> findByNameContainingIgnoreCase(String name);

}
