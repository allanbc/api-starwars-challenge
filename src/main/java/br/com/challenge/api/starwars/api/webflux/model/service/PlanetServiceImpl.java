package br.com.challenge.api.starwars.api.webflux.model.service;

import org.springframework.stereotype.Service;

import br.com.challenge.api.starwars.api.webflux.client.SwapiClient;
import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetCreation;
import br.com.challenge.api.starwars.api.webflux.model.exception.NotFoundException;
import br.com.challenge.api.starwars.api.webflux.model.repository.PlanetRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PlanetServiceImpl implements PlanetService {
	
	private final PlanetRepository repository;
	private final SwapiClient swapiClient;
	
	public PlanetServiceImpl(PlanetRepository repository, SwapiClient swapiClient) {
		this.repository = repository;
		this.swapiClient = swapiClient;
	}

	@Override
	public Mono<Planet> create(PlanetCreation planetCreation) {
		return swapiClient.findByName(planetCreation.getName())
//				.flatMapIterable(result -> result.getResults())
//				.switchIfEmpty(Mono.error(new NotFoundException("Planet name not found!")))
//				.flatMapIterable(films -> films.getFilms())
//				.count()
//				.doOnNext(planetCreation::setNumberAppearancesFilms)
//				.flatMap(ignore -> repository.save(planetCreation.requestPlanetBuilder()));
				.flatMapMany(result -> Flux.fromIterable(result.getResults()))
				.switchIfEmpty(Mono.error(new NotFoundException("Planet name not found!")))
				.flatMap(resultFilms -> Flux.fromIterable(resultFilms.getFilms()))
				.count()
				.doOnNext(planetCreation::setNumberAppearancesFilms)
				.flatMap(ignore -> repository.save(planetCreation.requestPlanetBuilder()));
	}

	@Override
	public Mono<Planet> findById(String id) {
		return repository.findById(id)
				.switchIfEmpty(Mono.error(new NotFoundException("Planet not found!")));
	}

	@Override
	public Flux<Planet> findByName(String name) {

		return repository.findByNameContainingIgnoreCase(name);
	}

	@Override
	public Flux<Planet> findAll() {

		return repository.findAll();
	}

	@Override
	public Mono<Void> delete(String id) {
		return findById(id)
				.flatMap(p -> repository.deleteById(p.getId()));
	}

}
