package br.com.challenge.api.starwars.api.webflux.factory;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import br.com.challenge.api.starwars.api.webflux.model.service.PlanetService;
import reactor.core.publisher.Flux;

@Component
public class PlanetFindFactory {
	
	private final PlanetService service;

	public PlanetFindFactory(PlanetService service) {
		this.service = service;
	}
	
	public Flux<Planet> find(ServerRequest request) {
		return request.queryParam("name")
				.map(service::findByName)
				.orElseGet(service::findAll);
	}
}
