package br.com.challenge.api.starwars.api.webflux.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.challenge.api.starwars.api.webflux.client.SwapiClient;
import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetResultDTO;
import br.com.challenge.api.starwars.api.webflux.model.dto.SwapiPlanetDTO;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/swapi/planets")
public class SwapiPlanetController {
	
	private final SwapiClient swapiClient;

	public SwapiPlanetController(SwapiClient swapiClient) {
		this.swapiClient = swapiClient;
	}
	
	@GetMapping
	public Flux<PlanetResultDTO<SwapiPlanetDTO>> find() {
		return swapiClient.findAll();
	}

}
