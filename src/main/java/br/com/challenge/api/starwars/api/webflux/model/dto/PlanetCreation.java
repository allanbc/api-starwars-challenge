package br.com.challenge.api.starwars.api.webflux.model.dto;

import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanetCreation {
	
	private String id;
	
	private String name;
	
	private String climate;
	
	private String terrain;
	
	private Long numberAppearancesFilms;
	
	@Builder.Default
	private String createDate = LocalDateTime.now().toString();
	
	private String updated;
	
	public Planet requestPlanetBuilder() {
		String uuid = UUID.randomUUID().toString();
		return Planet.builder()
				.id(uuid)
				.name(name)
				.climate(climate)
				.terrain(terrain)
				.numberAppearancesFilms(numberAppearancesFilms)
				.createdAt(createDate)
				.lastModified(updated)
				.build();
	}

}
