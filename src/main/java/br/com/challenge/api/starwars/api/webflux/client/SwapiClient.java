package br.com.challenge.api.starwars.api.webflux.client;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetResultDTO;
import br.com.challenge.api.starwars.api.webflux.model.dto.SwapiPlanetDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class SwapiClient {
	
	private final WebClient swapiClient;

	public SwapiClient(WebClient swapiClient) {
		this.swapiClient = swapiClient;
	}
	
	public Flux<PlanetResultDTO<SwapiPlanetDTO>> findAll() {
		return swapiClient
				.get()
				.uri("")
				.accept(APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(new ParameterizedTypeReference<PlanetResultDTO<SwapiPlanetDTO>>() {});
	}
	
	public Mono<PlanetResultDTO<SwapiPlanetDTO>> findByName(String name) {
		return swapiClient
				.get()
				.uri("?name" + name)
				.accept(APPLICATION_JSON)
				.retrieve()
				.bodyToMono(new ParameterizedTypeReference<PlanetResultDTO<SwapiPlanetDTO>>() {});
	}

}
