package br.com.challenge.api.starwars.api.webflux.events;

import static org.springframework.web.reactive.function.server.ServerResponse.created;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import java.net.URI;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import br.com.challenge.api.starwars.api.webflux.factory.PlanetFindFactory;
import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetCreation;
import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetResponse;
import br.com.challenge.api.starwars.api.webflux.model.service.PlanetService;
import reactor.core.publisher.Mono;

@Component
public class PlanetHandler {
	
	private final PlanetService service;
	private final PlanetFindFactory findFactory;
	
	public PlanetHandler(PlanetService service, PlanetFindFactory findFactory) {
		this.service = service;
		this.findFactory = findFactory;
	}

	public Mono<ServerResponse> findAll(ServerRequest request){
		return ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(findFactory.find(request), PlanetResponse.class);
	}
	
	public Mono<ServerResponse> findById(ServerRequest request){
		String id = request.pathVariable("id");
		return ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.findById(id), Planet.class);
	}
	
	public Mono<ServerResponse> create(ServerRequest request){
		return request
				.bodyToMono(PlanetCreation.class)
				.flatMap(service::create)
				.map(PlanetResponse::new)
				.flatMap(this::urlResponse);
	}
	
	public Mono<ServerResponse> delete(ServerRequest request) {
		String id = request.pathVariable("id");
		return ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.delete(id), Void.class);
	}
	
	private Mono<ServerResponse> urlResponse(PlanetResponse planet) {
		return created(URI.create("/planets/" + planet.getName()))
//				.getId()))
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(planet);
	}

}
