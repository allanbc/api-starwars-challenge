package br.com.challenge.api.starwars.api.webflux.model.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import lombok.Data;

@Data
public class PlanetResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String climate;
	private String terrain;
	private Long numberAppearancesFilms;
	private List<String> films;
	
	public PlanetResponse(Planet planet) {
		BeanUtils.copyProperties(planet, this);
	}
	
	public static Page<PlanetResponse> converter(Page<Planet> planets) {
		return planets.map(PlanetResponse::new);
	}
}
