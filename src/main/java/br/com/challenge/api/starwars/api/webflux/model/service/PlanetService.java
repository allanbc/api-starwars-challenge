package br.com.challenge.api.starwars.api.webflux.model.service;

import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetCreation;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PlanetService {
	
	Mono<Planet> create(PlanetCreation planetCreation);
	
	Mono<Planet> findById(String id);
	
	Flux<Planet> findByName(String name);
	
	Flux<Planet> findAll();
	
	Mono<Void> delete(String id);

}
