package br.com.challenge.api.starwars.api.webflux.config;

import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.boot.web.embedded.netty.NettyServerCustomizer;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

import reactor.netty.http.server.HttpServer;

@Component
public class NettyWebServerFactoryPortCustomizer implements WebServerFactoryCustomizer<NettyReactiveWebServerFactory> {

	@Override
	public void customize(NettyReactiveWebServerFactory factory) {
		factory.addServerCustomizers(new PortCustomizer(8080));
	}
	
	private static class PortCustomizer implements NettyServerCustomizer {
		
		private final int port;

		public PortCustomizer(int port) {
			this.port = port;
		}

		@Override
		public HttpServer apply(HttpServer httpServer) {
			return httpServer.port(port);
		}
		
	}
}
