package br.com.challenge.api.starwars;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.web.reactive.function.client.WebClient;

@EnableMongoAuditing
@SpringBootApplication
public class ApiStarwarsChallengeApplication {
	
	@Value("${apistarwars.urlSwapiApi}")
	private String urlSwapApi;

	public static void main(String[] args) {
		SpringApplication.run(ApiStarwarsChallengeApplication.class, args);
	}
	
	@Bean
	WebClient clientSwapi() {
		return WebClient.create(urlSwapApi);
	}
	
}
