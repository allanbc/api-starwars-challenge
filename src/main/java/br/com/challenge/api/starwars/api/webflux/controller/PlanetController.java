package br.com.challenge.api.starwars.api.webflux.controller;

import java.time.Duration;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.challenge.api.starwars.api.webflux.model.document.Planet;
import br.com.challenge.api.starwars.api.webflux.model.dto.PlanetCreation;
import br.com.challenge.api.starwars.api.webflux.model.service.PlanetService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

//@RestController
@RequestMapping("planets")
public class PlanetController {
	
	private PlanetService service;

	public PlanetController(PlanetService service) {
		this.service = service;
	}
	
	@GetMapping
	public ResponseEntity<Flux<Planet>> findAll() {
		var planets = service.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(planets);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Mono<Planet>> findById(@PathVariable("id") String id){
		
		var response = service.findById(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping("/planetName")
	public ResponseEntity<Object> findByName(@RequestParam(required=false) String name){
		
		var response = service.findByName(name);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping
	public ResponseEntity<Mono<Planet>> create(@RequestBody PlanetCreation planetCreation) {
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.create(planetCreation));
		
	}
	
	@GetMapping(value="/webflux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<Tuple2<Long, Planet>> getPlanetsByWebflux(){

		System.out.println("---Start get Planets by WEBFLUX--- " + LocalDateTime.now());
		Flux<Long> interval = Flux.interval(Duration.ofSeconds(2));
        Flux<Planet> planetsFlux = service.findAll();

        return Flux.zip(interval, planetsFlux);
        
	}
}
